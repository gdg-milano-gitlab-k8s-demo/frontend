# Frontend

### [webMeetup #1] GDG Cloud Milano - Live Code: CI/CD con Gitlab-CI e Kubernetes

This app is the frontend microservice of the application developed during the [first webmeetup of GDG Cloud Milano](https://www.youtube.com/watch?v=IBNwrk24BLk).

![Frontend Page](./images/frontend.png)

## Install Dependencies

```
npm install
```

## Serve

```
ng serve
```
