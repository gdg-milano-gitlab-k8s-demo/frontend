FROM nginx:1.15

COPY ./dist/frontend /usr/share/nginx/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
