import { Component, Input } from '@angular/core';

@Component({
  selector: 'k8s-header',
  templateUrl: 'header.component.html'
})
export class HeaderComponent {
  // -----------------------------------------------------------------------//
  title = 'GDG Cloud Milano';
  gcloudUrl: string = 'https://cloud.google.com/';
}
