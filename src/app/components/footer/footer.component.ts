import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'k8s-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  telegramUrl: string = 'https://t.me/gdgcloudmilano';
  twitterUrl: string = 'https://twitter.com/gdgcloudmilano';
  linkedinUrl: string = 'https://www.linkedin.com/company/gdg-cloud-milano';
  youtubeUrl: string =
    'https://www.youtube.com/channel/UCs2Lulo9cfYrI5RHuDCQehQ';

  constructor() {}

  ngOnInit() {}
}
