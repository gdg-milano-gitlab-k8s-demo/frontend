import { Component, Input } from '@angular/core';

@Component({
  selector: 'k8s-main',
  templateUrl: 'main.component.html'
})
export class MainComponent {
  // -----------------------------------------------------------------------//
  @Input() angularVersion: string = '';
  @Input() materialVersion: string = '';
  // -----------------------------------------------------------------------//
  constructor() {}
}
