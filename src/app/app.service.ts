import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

export interface LikesInfo {
  likes: number;
  hostname: string;
  commit: string;
}

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private readonly url = environment.apiUrl;

  constructor(private readonly http: HttpClient) {}

  getInfo$(): Observable<LikesInfo> {
    return this.http.get<LikesInfo>(`${this.url}`);
  }

  like$() {
    return this.http.post<void>(`${this.url}/like`, {}).subscribe();
  }
}
