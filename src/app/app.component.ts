import { Component } from '@angular/core';
import { AppService } from './app.service';
import { interval } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';

import { VERSION as MaterialVersion } from '@angular/material';
import { VERSION as AngularVersion } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'GDG Cloud Milano';
  ngVersion: string = AngularVersion.full;
  matVersion: string = MaterialVersion.full;

  likeInfo$ = interval(1000).pipe(switchMap(() => this.appService.getInfo$()));

  likes$ = this.likeInfo$.pipe(map(info => info.likes));
  commit$ = this.likeInfo$.pipe(map(info => info.commit));
  hostname$ = this.likeInfo$.pipe(map(info => info.hostname));

  constructor(private readonly appService: AppService) {}

  like() {
    console.log('test');
    this.appService.like$();
  }
}
